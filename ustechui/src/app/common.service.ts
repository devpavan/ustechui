import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';


import { Observable, of, from } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  getPointsByslug(slugVal: any) {
    throw new Error("Method not implemented.");
  }

  private rootUrl = 'http://192.168.0.106:8000/';

  constructor(private http: HttpClient) {

  }

  getListOfTeams() {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "team/", { headers: reqHeader });
  }
  getListOfPlayers() {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "player/", { headers: reqHeader });
  }
  getPlyaerByTeam(slug) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "team/" + slug + "/", { headers: reqHeader });
  }
  getMatches(){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "fixtures/", { headers: reqHeader });
  }
  getPlyaerBySlug(slug) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "player/" + slug + "/", { headers: reqHeader });
  }

  saveTeams(data) {
    console.log(data);
    const reqHeader = new HttpHeaders({ 'Content-Type': 'multipart/form-data','Accept': 'application/json' });
    return this.http.post(this.rootUrl + 'team/', data, { headers: reqHeader });
  }
  updateTeams(data) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.put(this.rootUrl + 'team/', data, { headers: reqHeader });
  }
  savePlayer(data) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.post(this.rootUrl + 'player/', data, { headers: reqHeader });
  }
  updatePlayer(data) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.put(this.rootUrl + 'player/', data, { headers: reqHeader });
  }
  fixtures() {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.post(this.rootUrl + 'fixtures/',  { headers: reqHeader });
  }
  getPoints(){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "points/" , { headers: reqHeader });

  }
  getWinnerTeam(team_one,team_two){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "winner-team/"+team_one+"/"+team_two+"/" , { headers: reqHeader });

  }
  getPointsByteamSlug(slug){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "points/"+slug+"/" , { headers: reqHeader });
  }
  getTeamBetweenMatches(slug){
    
    const reqHeader = new HttpHeaders({ 'Content-Type': 'Application/json' });
    return this.http.get(this.rootUrl + "team-between-matches/"+slug+"/" , { headers: reqHeader });
  }
}

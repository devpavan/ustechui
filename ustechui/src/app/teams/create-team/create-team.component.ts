import { Component, OnInit } from '@angular/core';
import { Team } from './team.model';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from 'src/app/common.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit {

  constructor(private commonService:CommonService,private formBuilder: FormBuilder) { }
team:Team = new Team();
uploadForm: FormGroup; 
  ngOnInit() {
     this.uploadForm = this.formBuilder.group({
      team_name: '',
      club_state:'',
      logoURI:['']
    });
  }


  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
      this.team = {
       team_name:"",
       logoURI:null,
       club_state:""
        
      };
    }
  }
isLoader = false;

onFileChanged(event) {
  const file = <File>event.target.files[0]
  console.log(file)
  this.team.logoURI=file;
  this.uploadForm.get('logoURI').setValue(file);
}
 form:FormData=new FormData()

createTeam(form: NgForm){
 this.uploadForm.get('team_name').setValue(this.team.team_name)
 this.uploadForm.get('club_state').setValue(this.team.club_state)
   
    console.log(this.uploadForm)
  this.commonService.saveTeams(this.team).subscribe((data: any) => {
    this.isLoader = false;


    // this.actuals$ = data;
    console.log(data);
    // this.resetForm(form);


  },
    (err: HttpErrorResponse) => {
      this.isLoader = false;
      //this.toastr.error('Book Posting Failed..');

    });
  
}
}

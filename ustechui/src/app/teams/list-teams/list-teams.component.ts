import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-teams',
  templateUrl: './list-teams.component.html',
  styleUrls: ['./list-teams.component.css']
})
export class ListTeamsComponent implements OnInit {

  constructor(private commonService: CommonService, private router: Router) { }

  ngOnInit() {
    this.getListOfTeams();
  }

  baseURL = "http://192.168.0.106:8000"

  isTeam = false;
  isFixtures = false;
  isLoader = false;
  teams = [];
  isShow = false;
  getWinnerTeam() {
    this.isFixtures = true;
    this.isTeam = false;
    console.log("winner")
    this.isShow = !this.isShow

  }
  applyFixtures() {
    this.isFixtures = true;
    this.isTeam = false;
    this.commonService.fixtures().subscribe((data: any) => {
      this.isLoader = false;


      this.teams = data;

      // this.actuals$ = data;
      console.log(data);
      // this.resetForm(form);


    },
      (err: HttpErrorResponse) => {
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }

  getListOfTeams() {
    this.isTeam = true;
    this.isFixtures = false;
    this.isLoader = true;
    this.commonService.getListOfTeams().subscribe((data: any) => {
      this.isLoader = false;
      this.teams = data;
    },
      (err: HttpErrorResponse) => {
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }
}
